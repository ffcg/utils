/************************************************************************
     File:        Track.cpp

     Author:     
                  Michael Gleicher, gleicher@cs.wisc.edu
     Modifier
                  Yu-Chi Lai, yu-chi@cs.wisc.edu
     
     Comment:     Container for the "World"

						This provides a container for all of the "stuff" 
						in the world.

						It could have been all global variables, or it could 
						have just been
						contained in the window. The advantage of doing it 
						this way is that
						we might have multiple windows looking at the same 
						world. But, I don't	think we'll actually do that.

						See the readme for commentary on code style

     Platform:    Visio Studio.Net 2003/2005

*************************************************************************/

#include "Track.H"

//****************************************************************************
//
// * Constructor
//============================================================================
CTrack::
CTrack() : trainU(0)
//============================================================================
{
	CTrack((spline_t)0);
}

CTrack::
CTrack(spline_t curve) : trainU(0)
//============================================================================
{
	resetPoints();
	calculateTrack(curve);
}

//****************************************************************************
//
// * provide a default set of points
//============================================================================
void CTrack::
resetPoints()
//============================================================================
{

	points.clear();
	points.push_back(ControlPoint(Pnt3f(50,5,0)));
	points.push_back(ControlPoint(Pnt3f(0,5,50)));
	points.push_back(ControlPoint(Pnt3f(-50,5,0)));
	points.push_back(ControlPoint(Pnt3f(0,5,-50)));

	// we had better put the train back at the start of the track...
	trainU = 0.0;
}

//****************************************************************************
//
// * Handy utility to break a string into a list of words
//============================================================================
void breakString(char* str, std::vector<const char*>& words) 
//============================================================================
{
	// start with no words
	words.clear();

	// scan through the string, starting at the beginning
	char* p = str;

	// stop when we hit the end of the string
	while(*p) {
		// skip over leading whitespace - stop at the first character or end of string
		while (*p && *p<=' ') p++;

		// now we're pointing at the first thing after the spaces
		// make sure its not a comment, and that we're not at the end of the string
		// (that's actually the same thing)
		if (! (*p) || *p == '#')
		break;

		// so we're pointing at a word! add it to the word list
		words.push_back(p);

		// now find the end of the word
		while(*p > ' ') p++;	// stop at space or end of string

		// if its ethe end of the string, we're done
		if (! *p) break;

		// otherwise, turn this space into and end of string (to end the word)
		// and keep going
		*p = 0;
		p++;
	}
}

//****************************************************************************
//
// * The file format is simple
//   first line: an integer with the number of control points
//	  other lines: one line per control point
//   either 3 (X,Y,Z) numbers on the line, or 6 numbers (X,Y,Z, orientation)
//============================================================================
void CTrack::
readPoints(const char* filename)
//============================================================================
{
	FILE* fp = fopen(filename,"r");
	if (!fp) {
		printf("Can't Open File!\n");
	} 
	else {
		char buf[512];

		// first line = number of points
		fgets(buf,512,fp);
		size_t npts = (size_t) atoi(buf);

		if( (npts<4) || (npts>65535)) {
			printf("Illegal Number of Points Specified in File");
		} else {
			points.clear();
			// get lines until EOF or we have enough points
			while( (points.size() < npts) && fgets(buf,512,fp) ) {
				Pnt3f pos,orient;
				vector<const char*> words;
				breakString(buf,words);
				if (words.size() >= 3) {
					pos.x = (float) strtod(words[0],0);
					pos.y = (float) strtod(words[1],0);
					pos.z = (float) strtod(words[2],0);
				} else {
					pos.x=0;
					pos.y=0;
					pos.z=0;
				}
				if (words.size() >= 6) {
					orient.x = (float) strtod(words[3],0);
					orient.y = (float) strtod(words[4],0);
					orient.z = (float) strtod(words[5],0);
				} else {
					orient.x = 0;
					orient.y = 1;
					orient.z = 0;
				}
				orient.normalize();
				points.push_back(ControlPoint(pos,orient));
			}
		}
		fclose(fp);
	}
	trainU = 0;
}

//****************************************************************************
//
// * write the control points to our simple format
//============================================================================
void CTrack::
writePoints(const char* filename)
//============================================================================
{
	FILE* fp = fopen(filename,"w");
	if (!fp) {
		printf("Can't open file for writing");
	} else {
		fprintf(fp,"%d\n",points.size());
		for(size_t i=0; i<points.size(); ++i)
			fprintf(fp,"%g %g %g %g %g %g\n",
				points[i].pos.x, points[i].pos.y, points[i].pos.z, 
				points[i].orient.x, points[i].orient.y, points[i].orient.z);
		fclose(fp);
	}
}

void CTrack::calculateTrack(spline_t type_spline){
	Matrix<double> G(3, 4);
	Matrix<double> M(4, 4);
	Matrix<double> T(4, 1);


	double M_value_cardinal[4][4] = { { -1.0, 2.0, -1.0, 0.0 }, { 3.0, -5.0, 0, 2 }, { -3, 4, 1, 0 }, { 1, -1, 0, 0 } };
	double M_value_Bspline[4][4] = { { -1, 3, -3, 1 }, { 3, -6, 0, 4 }, { -3, 3, 3, 1 }, { 1, 0, 0, 0 } };
	double G_value[3][4] = { 1 };
	double oG_value[3][4] = { 1 };
	double T_value[4][1] = { 1 };

	float percent = 1.0 / (float)DIVIDE_LINE;
	float t = 0.0;

	Pnt3f start;
	Pnt3f end;
	Pnt3f o_end;

	Vector3 dir;      // 行走方向
	Vector3 orient_v; // 車子向上方向
	Vector3 cross_t;  // 側邊方向 cross (dir ,orient)
	Matrix<double> result;
	Matrix<double> o_result;
	// 線性軌道的參數
	Pnt3f cp_pos_p1;
	Pnt3f cp_pos_p2;

	Pnt3f cp_orient_p1;
	Pnt3f cp_orient_p2;
	// 圓弧軌道參數
	Pnt3f G1;
	Pnt3f G2;
	Pnt3f G3;
	Pnt3f G4;

	Pnt3f o_G1;
	Pnt3f o_G2;
	Pnt3f o_G3;
	Pnt3f o_G4;
	Matrix<double> GM;
	Matrix<double> oGM;
	double K = 0.0;
	linePoints.clear();
	top_orients.clear();
	for (size_t j = 0; j < points.size(); j++)
	{
		t = 0.0f;
		switch (type_spline)
		{
		case spline_Linear:
			cp_pos_p1 = points[j].pos;
			cp_pos_p2 = points[(j + 1) % points.size()].pos;

			cp_orient_p1 = points[j].orient;
			cp_orient_p2 = points[(j + 1) % points.size()].orient;

			break;
		default:
			G1 = points[(j - 1) % points.size()].pos;
			G2 = points[j].pos;
			G3 = points[(j + 1) % points.size()].pos;
			G4 = points[(j + 2) % points.size()].pos;

			o_G1 = points[(j - 1) % points.size()].orient;
			o_G2 = points[j].orient;
			o_G3 = points[(j + 1) % points.size()].orient;
			o_G4 = points[(j + 2) % points.size()].orient;

			if (type_spline == spline_CardinalCubic)
			{
				M = &M_value_cardinal[0][0];
				K = 0.5;
			}
			else if (type_spline == spline_CubicB_Spline)
			{
				M = &M_value_Bspline[0][0];
				K = 1.0 / 6.0;
			}

			G_value[0][0] = G1.x; G_value[0][1] = G2.x; G_value[0][2] = G3.x; G_value[0][3] = G4.x;
			G_value[1][0] = G1.y; G_value[1][1] = G2.y; G_value[1][2] = G3.y; G_value[1][3] = G4.y;
			G_value[2][0] = G1.z; G_value[2][1] = G2.z; G_value[2][2] = G3.z; G_value[2][3] = G4.z;
			oG_value[0][0] = o_G1.x; oG_value[0][1] = o_G2.x; oG_value[0][2] = o_G3.x; oG_value[0][3] = o_G4.x;
			oG_value[1][0] = o_G1.y; oG_value[1][1] = o_G2.y; oG_value[1][2] = o_G3.y; oG_value[1][3] = o_G4.y;
			oG_value[2][0] = o_G1.z; oG_value[2][1] = o_G2.z; oG_value[2][2] = o_G3.z; oG_value[2][3] = o_G4.z;

			G = &G_value[0][0];

			GM = G*M*K;
			G = &oG_value[0][0];
			oGM = G*M*K;
			break;

		}

		for (size_t i = 0; i < DIVIDE_LINE; i++)
		{

			switch (type_spline)
			{
			case spline_Linear:
				if (i == 0)
				{
					start = (1 - t) * cp_pos_p1 + t * cp_pos_p2;
					linePoints.push_back(start);
					o_end = (1 - t) * cp_orient_p1 + t * cp_orient_p2;
					orient_v = Vector3(o_end.x, o_end.y, o_end.z);
					top_orients.push_back(orient_v);
				}
				t += percent;
				end = (1 - t) * cp_pos_p1 + t * cp_pos_p2;
				o_end = (1 - t) * cp_orient_p1 + t * cp_orient_p2;
				orient_v = Vector3(o_end.x, o_end.y, o_end.z);

				linePoints.push_back(end);
				top_orients.push_back(orient_v);

				start = end;

				break;
			default:

				// just for more effective
				if (i == 0)
				{
					T_value[0][0] = pow(t, 3); T_value[1][0] = pow(t, 2);
					T_value[2][0] = pow(t, 1); T_value[3][0] = 1.0;
					T = &T_value[0][0];

					result = GM*T;
					o_result = oGM*T;
					start = Pnt3f(result.get(0, 0), result.get(1, 0), result.get(2, 0));
					linePoints.push_back(start);
					end = Pnt3f(result.get(0, 0), result.get(1, 0), result.get(2, 0));
					o_end = Pnt3f(o_result.get(0, 0), o_result.get(1, 0), o_result.get(2, 0));
					orient_v = Vector3(o_end.x, o_end.y, o_end.z);
					top_orients.push_back(orient_v);
				}
				t += percent;
				T_value[0][0] = pow(t, 3); T_value[1][0] = pow(t, 2);
				T_value[2][0] = pow(t, 1); T_value[3][0] = 1.0;
				T = &T_value[0][0];
				result = GM*T;
				o_result = oGM*T;
				end = Pnt3f(result.get(0, 0), result.get(1, 0), result.get(2, 0));
				o_end = Pnt3f(o_result.get(0, 0), o_result.get(1, 0), o_result.get(2, 0));
				orient_v = Vector3(o_end.x, o_end.y, o_end.z);

				linePoints.push_back(end);
			    top_orients.push_back(orient_v);

				start = end;

				break;
			}
		}
	}
}
